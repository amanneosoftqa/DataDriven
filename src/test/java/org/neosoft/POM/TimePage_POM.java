package org.neosoft.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TimePage_POM 
{
	protected WebDriver driver;
	protected WebElement element = null;

	By Time = By.xpath("//a[@id='menu_time_viewTimeModule']/b");
	By projectInfo = By.xpath("//a[@id='menu_time_ProjectInfo']");
	By Customers = By.xpath("//a[@id='menu_time_viewCustomers']");
	By customerAddButton = By.xpath("//input[@id='btnAdd']");
	By AddcustomerName = By.xpath("//input[@name='addCustomer[customerName]']");
	By AddCustomerDescription = By.id("addCustomer_description");
	By savcustomerButton = By.xpath("//input[@id='btnSave']");
	By Projects = By.xpath("//a[@id='menu_time_viewProjects']");
	By AddProjectButton = By.xpath("//*[@id='btnAdd']");
	By ProjectCustomerName = By.xpath(".//*[@id='addProject_customerName']");
	By ProjectName = By.xpath(".//*[@id='addProject_projectName']");
	By ProjectAdmin = By.xpath(".//*[@id='addProject_projectAdmin_1']");
	By PrjectDescription = By.xpath(".//*[@id='addProject_description']");
	By ProjectSaveButton=By.xpath(".//*[@id='btnSave']");

	public TimePage_POM(WebDriver driver) 
	{
		this.driver = driver;
	}
	public WebElement time(WebDriver driver)
	{
		element = driver.findElement(Time);

		return element;
	}
	public WebElement projectinfo(WebDriver driver) 
{
		element = driver.findElement(projectInfo);

		return element;
}
	public WebElement customers(WebDriver driver)
	{
		element = driver.findElement(Customers);

		return element;
	}
	public WebElement customerAdd(WebDriver driver)
	{
		element = driver.findElement(customerAddButton);
		return element;
	}
	public WebElement customerName(WebDriver driver)
	{
		element = driver.findElement(AddcustomerName);
		return element;
	}
	public WebElement customerDescription(WebDriver driver) 
	{
		element = driver.findElement(AddCustomerDescription);
		return element;
	}
	public WebElement SaveButton(WebDriver driver)
	{
		element = driver.findElement(savcustomerButton);
		return element;
	}
	public WebElement Addarojectbutton(WebDriver driver)
	{
		element = driver.findElement(AddProjectButton);
		return element;
	}
	public WebElement ProjectcustomerName(WebDriver driver)
	{
		element = driver.findElement(ProjectCustomerName);
		return element;
	}	
	public WebElement projectName(WebDriver driver)
	{
		element=driver.findElement(ProjectName);
		return element;
	}
	public WebElement projectAdmin(WebDriver driver)
	{
		element=driver.findElement(ProjectAdmin);
		return element;
	}
	public WebElement prjectDescription(WebDriver driver)

	{
		element=driver.findElement(PrjectDescription);
		return element;
	}	
	public WebElement Projectsavebutton(WebDriver driver)
	{
		element=driver.findElement(ProjectSaveButton);
		return element;
	}
	
}
