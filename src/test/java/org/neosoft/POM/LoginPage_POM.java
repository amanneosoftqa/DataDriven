package org.neosoft.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage_POM
{
	protected WebDriver driver;
	protected WebElement element;
		
	By username=By.xpath("//input[@id='txtUsername']");
	By password=By.xpath("//input[@id='txtPassword']");
	By loginButton=By.xpath("//input[@name='Submit']");
	
	
	public WebElement login_username(WebDriver driver)
	{
		WebElement element=driver.findElement(username);
		return element;
	}
	public WebElement login_password(WebDriver driver)
	{
		element= driver.findElement(password);

		return element;
	}
	public WebElement login_BTN(WebDriver driver)
	{

		element = driver.findElement(loginButton);

		return element;
	}
}
